import { Server, Socket } from "socket.io"
import { IPlayer } from "../../entities/Player/IPlayer"
import { Player } from "../../entities/Player/Player"
import { Room } from "../../entities/Room/Room"
import { chronoEvents, playerEvents, roomEvents, teamEvents, wordEvent } from "../Events"
import { ioErrorHandler } from "../../errorHandlers/IoErrorHandler"
import { GameStep, IRoom } from "../../entities/Room/IRoom"
import { Word } from "../../entities/Word/Word"
import playerModel from "../../models/PlayerModel"
import { Team } from "../../entities/Team/Team"
import { ITeam } from "../../entities/Team/ITeam"
import { IWord } from "../../entities/Word/IWord"
import teamModel from "../../models/TeamModel"
import { UnplainedError } from "../../entities/UnplainedError"
import { Chrono } from "../../entities/Chrono/Chrono"
import { IChrono } from "../../entities/Chrono/IChrono"
import { DomainError } from "../../entities/DomainError"
import wordModel from "../../models/WordModel"

type Message = {
  type: "error" | "success",
  content: string
}

class InGameHandler {

  static changeType = (io: Server, socket: Socket) => async (
    type: IChrono["type"],
    chronoId: string,
    roomId: string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      await Room.checkRoomState(roomId, { gameStep: GameStep.wordGuessing })
      const chrono = await Chrono.setType(type, chronoId, roomId)
      io.to(roomId).emit(chronoEvents.server.TYPE_CHANGE, ...[chrono])
      await InGameHandler.resetChrono(io, socket)(chronoId, roomId)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static resetChrono = (io: Server, socket: Socket) => async (
    chronoId: string,
    roomId: string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      await Room.checkRoomState(roomId, { gameStep: GameStep.wordGuessing })
      const chrono = await Chrono.reset(chronoId, roomId)

      io.to(roomId).emit(chronoEvents.server.RESETED, ...[chrono])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static incrementChrono = (io: Server, socket: Socket) => async (
    milliseconds : number,
    chronoId: string,
    roomId: string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      await Room.checkRoomState(roomId, { gameStep: GameStep.wordGuessing })
      const chrono = await Chrono.incrementCountDown(milliseconds, chronoId, roomId)

      io.to(roomId).emit(chronoEvents.server.INCREMENTED, ...[chrono])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static toggleChronoState = (io: Server, socket: Socket) => async (
    chronoState : IChrono["state"],
    chronoId: string,
    roomId: string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      await Room.checkRoomState(roomId, { gameStep: GameStep.wordGuessing })
      if (chronoState === "play"){
        const chrono = await Chrono.play(chronoId, roomId)
        io.to(roomId).emit(chronoEvents.server.PLAY, ...[chrono])
      }
      else if (chronoState === "pause") {
        const chrono = await Chrono.pause(chronoId, roomId)
        io.to(roomId).to(roomId).emit(chronoEvents.server.PAUSE, ...[chrono])
      }
      else {
        throw DomainError.message("Chrono update refused: invalid state")
      }

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static getTeam = (io: Server, socket: Socket) => async (
    teamId: string,
    roomId: string,
    callback : (team: ITeam) => void
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      const team = await Team.checkTeamState(
        teamId,
        { roomId },
        "Team not found"
      ) as ITeam

      callback(team)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static selectGuesser = (io: Server, socket: Socket) => async (
    blindedPlayerId: string,
    roomId: string
  ) => {

    try {
      await Player.isPlayerTurn(socket.id, roomId)
      const { blindedPlayer, unblindedPlayer } = await Player.blind(blindedPlayerId, { roomId })
      const args : IPlayer[] = [blindedPlayer]
      if (unblindedPlayer)
        args.push(unblindedPlayer)

      io.to(roomId).emit(playerEvents.server.BLINDED, ...args)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static nextStep = (io: Server, socket: Socket) => async (roomId: string) => {

    try {
      await Player.isPlayerTurn(socket.id, roomId)
      const room : IRoom = await Room.nextStep(roomId)
      io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[room])
      await InGameHandler.stepUpdateSideEffect(io, socket)(room)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static goToStep = (io: Server, socket: Socket) => async (step: string, roomId: string) => {

    try {
      await Player.isPlayerTurn(socket.id, roomId)
      const room : IRoom = await Room.goToStep(step, roomId)
      io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[room])
      await InGameHandler.stepUpdateSideEffect(io, socket)(room)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static getWord = (io: Server, socket: Socket) => async (
    wordId: string,
    roomId : string,
    callback : (wrd: IWord) => void
  ) => {

    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      const word = await Word.get(wordId, roomId)
      callback(word)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static changeWord = (io: Server, socket: Socket) => async (
    teamId: string, roomId: string
  ) => {

    try {
      const leaderCanChangeWord = true
      await Player.isPlayerTurn(socket.id, roomId, {}, leaderCanChangeWord)
      const filter = leaderCanChangeWord
        ? { gameStep: GameStep.wordSelection }
        : { leader: socket.id, gameStep: GameStep.wordSelection }
      await Room.checkRoomState(
        roomId,
        filter,
        "You cannot change word"
      )
      const { word: newWord, team } = await Word.change(teamId, roomId)
      io.to(roomId).emit(wordEvent.server.CHANGED, ...[newWord, team])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static voteWord = (io: Server, socket: Socket) => async (
    vote: number, wordId: string, roomId: string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      const room = await Room.checkRoomState(
        roomId,
        { gameStep: GameStep.wordSelection },
        "It's not time to vote"
      ) as IRoom

      const team = await Team.checkTeamState(
        room.teamTurn,
        { wordId, roomId },
        "This word doesn't belong to this team"
      ) as ITeam

      const player : IPlayer = await Player.vote(socket.id, vote, {
        $or: [
          { blinded: { $ne: true } },
          { teamId: { $ne: room.teamTurn } }
        ]
      })

      io.to(roomId).emit(playerEvents.server.VOTED, ...[player])

      const players : IPlayer[] = await playerModel.find({ roomId })
      const missingPlayers : number = players.filter(player => player.vote === 0).length

      if (missingPlayers > 1)
        return

      const voteUp = players.filter(player => player.vote === 1).length
      const voteDown = players.filter(player => player.vote === -1).length
      const isLoose = voteDown > voteUp

      const resetedPlayers : IPlayer[] = await Player.resetVotes(roomId)
      const message : Message = isLoose
        ? { type: "error", content: "NOPE !! WORD REFUSED" }
        : { type: "success", content: "OOOK ! WORD ACCEPTED" }

      io.to(roomId).emit(playerEvents.server.RESET_VOTE, ...[resetedPlayers, message])

      if (isLoose){
        await Word.unvalidate(wordId)
        const { team: teamUpdated, word } = await Word.change(team.id, roomId)
        io.to(roomId).emit(wordEvent.server.CHANGED, ...[word, teamUpdated])
        return
      }

      const word = await Word.validate(wordId)
      const roomUpdated = await Room.nextStep(roomId)
      io.to(roomId).emit(wordEvent.server.VALIDATED, ...[word])
      io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[roomUpdated])

    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static wordGuessed = (io: Server, socket: Socket) => async (
    wordId: string, roomId: string
  ) => {
    try {
      const player = await Player.isPlayerTurn(socket.id, roomId)
      await Room.checkRoomState(
        roomId,
        { gameStep: GameStep.wordGuessing },
        "It's no time to guess a word !"
      )
      await Team.checkTeamState(
        roomId,
        { id: player.teamId, wordId: wordId },
        "This word does't belong your team ..."
      )

      const { word, team: teamWordGuessed } = await Word.guessed(wordId, roomId, player.teamId)
      io.to(roomId).emit(wordEvent.server.GUESSED, ...[word, teamWordGuessed])

      // const { word: newWord, team: teamNewWord } = await Word.change(player.teamId, roomId)
      // io.to(roomId).emit(wordEvent.server.CHANGED, ...[newWord, teamNewWord])

      const room = await Room.nextStep(roomId)
      io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[room])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static turnEnd = (io: Server, socket: Socket) => async (
    teamId: string, roomId : string
  ) => {

    try {
      await Player.isPlayerTurn(socket.id, roomId, {
        teamId
      })

      const { team: prevTeam, room } = await Team.turnEnd(teamId, roomId)
      io.to(roomId).emit(teamEvents.server.TURN_ENDED, ...[prevTeam, room])

      const roundIsFinished = await Room.isRoundFinished(roomId)

      /*---Case: Round continue ---*/
      if (!roundIsFinished){
        await InGameHandler.gameNextTeam(io, socket)(roomId)
        return
      }
      const gameIsFinished = await Room.gameIsFinished(roomId, "all")

      /*---Case: End Game---*/
      if (gameIsFinished){
        const roomEndGame = await Room.goToStep(GameStep.postGame, roomId)
        io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[roomEndGame])
        await InGameHandler.stepUpdateSideEffect(io, socket)(room)
        return
      }

      /*---Case: New Round---*/
      const teams = await Team.resetPlayed(roomId)
      const roomNewRound = await Room.nextRound(roomId)
      io.to(roomId).emit(roomEvents.server.NEXT_ROUND, ...[roomNewRound, teams])

      await InGameHandler.gameNextTeam(io, socket)(roomId)
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  static gameNextTeam = (io: Server, socket: Socket) => async (roomId : string) => {

    const { room } = await Team.nextTeam(roomId)
    io.to(roomId).emit(roomEvents.server.NEXT_TEAM, ...[room])

    const roomNextTeam = await Room.goToStep(GameStep.guesserSelection, roomId)
    io.to(roomId).emit(roomEvents.server.STEP_CHANGED, ...[roomNextTeam])

    await InGameHandler.stepUpdateSideEffect(io, socket)(roomNextTeam)
    return
  }

  static updateScore = (io: Server, socket: Socket) => async (
    score : number, teamId: string, roomId:string
  ) => {
    try {
      await Player.playerBelongTheRoom(socket.id, roomId)
      await Room.checkRoomState(
        roomId,
        { gameStep: { $ne: GameStep.postGame } },
        "Game is finished: Can't anymore update score"
      )
      const team = await Team.updateScore(score, teamId, roomId)
      io.to(team.roomId).emit(teamEvents.server.SCORE_UPDATED, ...[team])
    }
    catch (err) {
      ioErrorHandler(err, socket)
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  static stepUpdateSideEffect = (io: Server, socket: Socket) => async (
    room: IRoom
  ) => {

    const sendNewWord = async (teamId: string, roomId: string) => {
      const { team, word } = await Word.change(teamId, roomId)
      setTimeout(()=>{
        io.to(team.roomId).emit(wordEvent.server.CHANGED, ...[word, team])
      }, 500)
    }

    const step : GameStep = room.gameStep
    switch (step){
      case GameStep.wordSelection: {
        const teamTurn : ITeam | null = await teamModel.findOne({ id: room.teamTurn })
        if (!teamTurn)
          throw new UnplainedError("Internal server error: Word init failed")
        if (!teamTurn.wordId)
          return sendNewWord(teamTurn.id, room.id)

        const word : IWord | null = await wordModel.findOne({ id: teamTurn.wordId })
        if (!word)
          throw new UnplainedError("Internal server error: Word unfound")

        if (word.guessed)
          return sendNewWord(teamTurn.id, room.id)
      }
    }
  }

}

function initInGameHandler (io: Server, socket: Socket) {

  socket.on(teamEvents.client.GET, InGameHandler.getTeam(io, socket))
  socket.on(teamEvents.client.UPDATE_SCORE, InGameHandler.updateScore(io, socket))
  socket.on(teamEvents.client.TURN_END, InGameHandler.turnEnd(io, socket))
  socket.on(wordEvent.client.GUESSED, InGameHandler.wordGuessed(io, socket))
  socket.on(wordEvent.client.GET, InGameHandler.getWord(io, socket))
  socket.on(wordEvent.client.CHANGE, InGameHandler.changeWord(io, socket))
  socket.on(playerEvents.client.VOTE_WORD, InGameHandler.voteWord(io, socket))
  socket.on(roomEvents.client.NEXT_STEP, InGameHandler.nextStep(io, socket))
  socket.on(roomEvents.client.GO_TO_STEP, InGameHandler.goToStep(io, socket))
  socket.on(playerEvents.client.BLIND, InGameHandler.selectGuesser(io, socket))
  socket.on(chronoEvents.client.RESET, InGameHandler.resetChrono(io, socket))
  socket.on(chronoEvents.client.INCREMENT, InGameHandler.incrementChrono(io, socket))
  socket.on(chronoEvents.client.TOGGLE_STATE, InGameHandler.toggleChronoState(io, socket))
  socket.on(chronoEvents.client.CHANGE_TYPE, InGameHandler.changeType(io, socket))

}

export { initInGameHandler, InGameHandler }
