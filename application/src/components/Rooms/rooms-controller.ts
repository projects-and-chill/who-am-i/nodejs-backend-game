import { Room } from "../../entities/Room/Room"
import { IRoom } from "../../entities/Room/IRoom"

export const rooms = async (req, res, next) => {

  try {

    const rooms: Pick<IRoom, "id" | "name" | "avatar">[] = await Room.list()
    res.status(200).send({ data: rooms })

  }
  catch (error) {

    return next(error)

  }

}

export const room = async (req, res, next) => {

  try {

    res.status(200).send()

  }
  catch (error) {

    return next(error)

  }

}
