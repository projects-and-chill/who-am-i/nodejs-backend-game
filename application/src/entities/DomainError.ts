export class DomainError extends Error {

  public type: "warning" = "warning"
  public location: string

  static message (message: string, location?: string){
    if (location)
      console.log("ERROR LOCATION  : ", location)
    return new DomainError(message, "unkown")
  }

  get error () {
    return { message: this.message, type: this.type }
  }

  private constructor (
    message: string, callerTrace: string
  ) {
    super(message)
    this.location = callerTrace
    this.name = "DomainError"
  }

}
