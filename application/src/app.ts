import express, { Express } from "express"
import { Router } from "express/ts4.0"
import cookieParser from "cookie-parser"
import bodyParser from "body-parser"

class ExpressApp {

  private app = express()

  constructor (private router: Router, private apiName: string = "") {}

  private setHeaders () {

    this.app.use((req, res, next) => {

      res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
      )
      res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PUT, DELETE, PATCH, OPTIONS"
      )
      res.setHeader("Access-Control-Allow-Origin", "*")
      res.setHeader("Accept", "application/json")
      next()

    })

  }

  private setParsers () {

    this.app.use(bodyParser.urlencoded({ extended: false }))
    this.app.use(bodyParser.json())
    this.app.use(cookieParser())

  }

  public init (): Express {

    this.setHeaders()
    this.setParsers()
    this.app.use(`/${this.apiName}`, this.router)
    return this.app

  }

}

export { ExpressApp }
