import { model, Schema } from "mongoose"
import { IRoom } from "../entities/Room/IRoom"

const stringValidator = (str) => {

  return typeof str === "string"

}

const rulesSchema = new Schema<IRoom["rules"]>({
  wordQty: { type: Number, required: true },
  time: { type: [Schema.Types.Mixed], required: true },
  endGame: { type: [Schema.Types.Mixed], required: true },
  podium: { type: String, required: true }
})

const schema = new Schema<IRoom>({
  id: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  rules: rulesSchema,
  started: { type: Boolean, required: true },
  protected: { type: Boolean, required: true },
  open: { type: Boolean, required: true },
  leader: { type: String, validate: stringValidator, default: "" },
  timeMaster: { type: String, validate: stringValidator, default: "" },
  round: { type: Number, required: true },
  teamTurn: { type: String, validate: stringValidator, default: "" },
  gameStep: { type: String, required: true },
  avatar: { type: String, required: true }
})

const roomModel = model<IRoom>("rooms", schema)
export default roomModel
