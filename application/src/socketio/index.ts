import { Server } from "socket.io"
import { Server as httpServer } from "http"
import { disconnectionHandler } from "./handlers/5-disconnectionHandler"
import { initLobbyHandler } from "./handlers/1-lobbyHandler"
import { initPreGameHandler } from "./handlers/2-preGameHandler"
import { initInGameHandler } from "./handlers/3-inGameHandler"

function socketioInit (server: httpServer) {

  const io: Server = new Server(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
  })

  const onConnection = (socket) => {
    console.log("a user connected:", socket.id)
    initLobbyHandler(io, socket)
    initPreGameHandler(io, socket)
    initInGameHandler(io, socket)
    disconnectionHandler(io, socket)
  }
  io.on("connection", onConnection)
}

export default socketioInit
