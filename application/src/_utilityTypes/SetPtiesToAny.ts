export type SetPtiesToAny<T extends Record<string, any>> = Record<keyof T, any>
