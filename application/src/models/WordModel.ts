import { model, Schema } from "mongoose"
import { IWord } from "../entities/Word/IWord"

const schema = new Schema<IWord>({
  id: { type: String, required: true, unique: true },
  value: { type: String, required: true },
  owner: { type: String, required: true },
  roomId: { type: String, required: true },
  guessed: { type: Boolean, required: true },
  validate: { type: Number, required: true },
  used: { type: Boolean, required: true }

})

const wordModel = model<IWord>("words", schema)
export default wordModel
