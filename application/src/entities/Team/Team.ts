import { Entity } from "../Entity"
import { ITeam } from "./ITeam"
import teamModel from "../../models/TeamModel"
import playerModel from "../../models/PlayerModel"
import roomModel from "../../models/RoomModel"
import { DomainError } from "../DomainError"
import { IRoom } from "../Room/IRoom"
import { IPlayer } from "../Player/IPlayer"
import { UnplainedError } from "../UnplainedError"

export class Team extends Entity<ITeam> {

  static randomColor = () => {

    let color: string = ""
    while (color.length !== 6)
      color = Math.floor(Math.random() * 16777215).toString(16)
    return "#" + color

  }

  static template: ITeam = {
    id: "",
    guessedWords: [],
    played: false,
    color: "",
    playOrder: -1,
    wordId: "",
    score: 0,
    roomId: ""
  }

  static async checkTeamState (
    teamId: string,
    filters: Partial<ITeam> = {},
    message?: string
  ): Promise<ITeam | null> {

    const filterBug : any = {
      id: teamId,
      ...filters
    }

    const team : ITeam | null = await teamModel.findOne(filterBug)

    if (!team && message)
      throw DomainError.message(message)

    return team
  }

  static async create (
    roomId: string,
    roomCheck: Partial<IRoom> = {},
    playerCheck: Partial<IPlayer> = {}
  ): Promise<ITeam> {

    const roomExist = await roomModel.exists({ id: roomId, ...roomCheck })

    if (!roomExist) {
      throw DomainError.message(
        "Team creation refused: The room does not meet the necessary conditions"
      )
    }

    let playerExist: boolean = true
    if (Object.keys(playerCheck).length)
      playerExist = !!(await playerModel.exists(playerCheck))

    if (!playerExist) {
      throw DomainError.message(
        "Team creation refused: You don't meet the necessary conditions"
      )
    }

    const team: ITeam = {
      ...Team.template,
      id: Team.generateId("t"),
      roomId,
      color: Team.randomColor()
    }
    await new teamModel(team).save()
    return team

  }

  static async checkTeamPlayers (
    teamId: string,
    shouldDeleteTeam: boolean = true,
    ignorePlayerId?: string
  ): Promise<boolean> {

    const filter = ignorePlayerId
      ? { teamId: teamId, id: { $ne: ignorePlayerId } }
      : { teamId: teamId }

    const count: number = await playerModel.count(filter)

    if (count !== 0)
      return false

    if (!shouldDeleteTeam)
      return true

    await Team.delete(teamId)
    return true

  }
  static async delete (teamId: string): Promise<void> {
    await teamModel.deleteOne({ id: teamId })
  }

  static async list (roomId: string): Promise<ITeam[]> {
    return teamModel.find({ roomId }).lean()
  }

  static async resetPlayed (roomId: string): Promise<ITeam[]> {

    await teamModel.updateMany({ roomId }, { played: false })

    const teams: ITeam[] = await teamModel.find({ roomId }).lean()
    if (!teams.length)
      throw new UnplainedError("There are no team in this room")
    return teams

  }

  static async definePlayOrder (roomId: string): Promise<ITeam[]> {

    const teams: ITeam[] = await teamModel.find({ roomId }, "-_id").lean()
    if (!teams.length)
      throw new UnplainedError("There are no team in this room")

    for (let i = teams.length - 1; i > 0; i--) {

      const j = Math.floor(Math.random() * (i + 1))
      const temp = teams[i]
      teams[i] = teams[j]
      teams[j] = temp

    }

    const teamsShuffled: ITeam[] = teams.map((team, index) => ({
      ...team,
      playOrder: index
    }))

    const bulkTeams = teamsShuffled.map((team) => ({
      updateOne: {
        filter: { id: team.id },
        update: { $set: { playOrder: team.playOrder } }
      }
    }))
    await teamModel.bulkWrite(bulkTeams)

    return teamsShuffled

  }

  static async turnEnd (teamId : string, roomId: string) : Promise<{ team: ITeam, room: IRoom }>{

    const team : ITeam | null = await teamModel.findOneAndUpdate(
      { id: teamId, roomId, played: false },
      { played: true }
    )
    if (!team)
      throw DomainError.message("Cannot end team turn")

    const room : IRoom | null = await roomModel.findOneAndUpdate(
      { id: roomId, started: true },
      { teamTurn: "" },
      { new: true }
    )
    if (!room)
      throw new UnplainedError("Internal server error: Room not found")

    return { team, room }
  }

  static async nextTeam (roomId: string) : Promise<{ room:IRoom, nextTeam:ITeam }> {

    const nextTeam: ITeam | null = await teamModel
      .findOne({ roomId: roomId, played: false })
      .sort({ playOrder: 1 })
      .lean()

    if (!nextTeam)
      throw DomainError.message("All team have already played")

    const room = await roomModel
      .findOneAndUpdate(
        { id: roomId, started: true },
        { teamTurn: nextTeam.id },
        { new: true }
      )
      .lean()

    if (!room)
      throw new UnplainedError("Internal server error : Impossible path, please restart game")

    return { room, nextTeam }
  }

  static async updateScore (score: number, teamId: string, roomId: string): Promise<ITeam> {

    const team : ITeam | null = await teamModel.findOneAndUpdate(
      { id: teamId, roomId },
      { score },
      { new: true }
    )

    if (!team)
      throw new UnplainedError("Cannot update score: Team doesn't exist")

    return team
  }

}
