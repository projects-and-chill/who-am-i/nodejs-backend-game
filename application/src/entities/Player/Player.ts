import { Entity } from "../Entity"
import { IPlayer } from "./IPlayer"
import playerModel from "../../models/PlayerModel"
import { DomainError } from "../DomainError"
import { UnplainedError } from "../UnplainedError"
import roomModel from "../../models/RoomModel"
import { IRoom } from "../Room/IRoom"
import teamModel from "../../models/TeamModel"

export class Player extends Entity<IPlayer> {

  static template: IPlayer = {
    id: "",
    roomId: "",
    username: "",
    teamId: "",
    blinded: false,
    vote: 0,
    avatar: ""
  }

  static async create (
    id: string,
    username: string,
    options: Partial<IPlayer> = {}
  ): Promise<IPlayer> {

    if (!username)
      throw DomainError.message("You should provide valide username")

    const player: IPlayer = {
      ...Player.template,
      id: id,
      username: username,
      avatar: `https://avatars.dicebear.com/api/adventurer/${username}.svg`,
      ...options
    }
    await new playerModel(player).save()
    return player

  }

  static async joinRoom (
    playerId: string,
    roomId: string,
    options: Partial<IPlayer> = {}
  ): Promise<{ player: IPlayer; room: IRoom }> {

    const room = await roomModel.findOne({ id: roomId, open: true })

    if (!room)
      throw DomainError.message("You cannot join this room")

    const player = await playerModel.findOneAndUpdate(
      { id: playerId },
      {
        roomId,
        ...options
      },
      { new: true }
    )

    if (!player)
      throw new UnplainedError("This player doesn't exist")

    return { player, room }

  }

  static async switchTeam (
    playerId: string,
    teamId: string,
    roomId: string
  ): Promise<{ player: IPlayer; prevTeamId: string }> {

    const teamExist = await teamModel.exists({ id: teamId, roomId })
    if (!teamExist)
      throw DomainError.message("Team switch refused: This team doesn't exist")

    const player: IPlayer | null = await playerModel
      .findOneAndUpdate(
        { id: playerId, roomId, teamId: { $ne: teamId } },
        { teamId },
        { new: false }
      )
      .lean()

    if (!player)
      throw DomainError.message("Team switch refused")

    return { player: { ...player, teamId: teamId }, prevTeamId: player.teamId }

  }

  static async delete (playerId: string): Promise<IPlayer | null> {

    return playerModel.findOneAndDelete({ id: playerId })

  }

  static async list (roomId: string): Promise<IPlayer[]> {

    return playerModel.find({ roomId }).lean()

  }

  static async vote (
    playerId: string,
    vote: number,
    filters: Partial<IPlayer>&Record<string, any> = {}
  ): Promise<IPlayer> {

    const allowedVotes : IPlayer["vote"][] = [-1, 0, 1]
    if (!allowedVotes.some(v => v === vote))
      throw DomainError.message("Vote refused: invalid vote content")

    const player: IPlayer | null = await playerModel
      .findOneAndUpdate({ id: playerId, ...filters }, { vote }, { new: true })
      .lean()

    if (!player)
      throw DomainError.message("You can't vote")

    return player

  }

  static async resetVotes (
    roomId: string,
    options: Partial<IRoom> = {}
  ): Promise<IPlayer[]> {

    const roomExist = await roomModel.findOne({ id: roomId, ...options })
    if (!roomExist)
      throw DomainError.message("Cannot reset votes for this room")

    await playerModel.updateMany({ roomId: roomId }, { vote: 0 })
    const players = await playerModel.find({ roomId: roomId })
    if (!roomExist)
      throw new UnplainedError("Cannot reset votes: No players found this room")
    return players

  }

  static async isPlayerTurn (
    playerId : string,
    roomId: string,
    filters : Partial<IPlayer>&Record<string, any> = {},
    acceptLeader = false
  ) : Promise<IPlayer>{

    const player : IPlayer | null = await playerModel.findOne({ id: playerId, roomId, ...filters })
    if (!player)
      throw DomainError.message("It's not your turn !")

    const room : IRoom | null = await roomModel.findOne({ id: roomId })
    if (!room)
      throw DomainError.message("Room doesn't exist")

    if (!acceptLeader && player.teamId !== room.teamTurn)
      throw DomainError.message("Is't not your turn !")

    return player
  }

  static async playerBelongTheRoom (playerId: string, roomId : string) : Promise<IPlayer>{

    const room : IRoom | null = await roomModel.findOne({ id: roomId })
    if (!room)
      throw new UnplainedError("Room doesn't exist")

    const player : IPlayer | null = await playerModel.findOne({ id: playerId, roomId })
    if (!player)
      throw new UnplainedError("You don't belong the room")

    return player
  }

  static async blind (
    playerId: string,
    filters: Partial<IPlayer> = {}
  ): Promise<{ blindedPlayer : IPlayer, unblindedPlayer : IPlayer|null }> {

    const blindedPlayer : IPlayer | null = await playerModel.findOneAndUpdate(
      { id: playerId, blinded: false, ...filters },
      { blinded: true },
      { new: true }
    ).lean()

    if (!blindedPlayer)
      throw DomainError.message("Cannot blind player")

    const unblindedPlayer : IPlayer | null = await playerModel.findOneAndUpdate(
      {
        id: { $ne: playerId },
        blinded: true,
        roomId: blindedPlayer.roomId,
        teamId: blindedPlayer.teamId
      },
      { blinded: false },
      { new: true }
    ).lean()

    return { blindedPlayer, unblindedPlayer }

  }

}
