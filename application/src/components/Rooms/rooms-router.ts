import express from "express"
import { rooms } from "./rooms-controller"

const router = express.Router()

router.get("/", rooms)

export default router
