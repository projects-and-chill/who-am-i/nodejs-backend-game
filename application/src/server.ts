import { dbConnexion } from "./_services/MongoConnect/mongoConnect"
import { IMongoConnect } from "./_services/MongoConnect/IMongoConnect"
import { ExpressApp } from "./app"
import { rootRouter } from "./routes"
import { createServer } from "http"
import socketioInit from "./socketio"

const run = async () => {

  const { API_PORT } = process.env
  const dbInfos: IMongoConnect = {
    hostname: process.env.MONGO_HOSTNAME as string,
    port: process.env.MONGO_PORT as string,
    dbName: process.env.MONGO_API_DB as string,
    authSource: process.env.MONGO_AUTH_SOURCE as string,
    user: process.env.MONGO_API_DB_USERNAME as string,
    pwd: process.env.MONGO_API_DB_PASSWORD as string
  }

  await dbConnexion(dbInfos)

  const API_NAME = process.env.API_NAME as string
  const app = new ExpressApp(rootRouter, API_NAME).init()
  const httpServer = createServer(app)

  socketioInit(httpServer)

  httpServer.listen(API_PORT, () => {

    console.log(`Mon nodejs ecoute sur le port : "${API_PORT as string}" `)
    console.log(`Le nom de l'api est ${API_NAME}`)

  })

}

run()
  .then(() => console.log("SERVER OK"))
  .catch((err) => console.log("PROBLEM", err))
