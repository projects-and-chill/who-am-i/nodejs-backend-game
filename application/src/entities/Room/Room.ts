import { Entity } from "../Entity"
import { GameStep, IRoom } from "./IRoom"
import roomModel from "../../models/RoomModel"
import { DomainError } from "../DomainError"
import { UnplainedError } from "../UnplainedError"
import playerModel from "../../models/PlayerModel"
import { IPlayer } from "../Player/IPlayer"
import teamModel from "../../models/TeamModel"
import { ITeam } from "../Team/ITeam"
import { SetPtiesToAny } from "../../_utilityTypes/SetPtiesToAny"

export class Room extends Entity<IRoom> {

  static template: IRoom = {
    id: "",
    leader: "",
    timeMaster: "",
    name: "",
    rules: {
      wordQty: 3,
      time: ["countdown", 45],
      endGame: ["rounds", 5],
      podium: "ascending"
    },
    round: 0,
    open: true,
    started: false,
    gameStep: GameStep.preGame,
    teamTurn: "",
    protected: false,
    avatar: ""
  }

  static async create (
    roomName: string,
    playerId: string,
    options: Partial<IRoom> = {}
  ): Promise<IRoom> {

    if (!roomName)
      throw DomainError.message("You should provide roomName")

    if (!(await playerModel.exists({ id: playerId })))
      throw DomainError.message("You should create player before create room")

    const roomId: string = Room.generateId("r")

    const room: IRoom = {
      ...Room.template,
      id: roomId,
      name: roomName,
      leader: playerId,
      timeMaster: playerId,
      avatar: `https://avatars.dicebear.com/api/identicon/${roomId}.svg`,
      ...options
    }

    await playerModel.updateOne({ id: playerId }, { roomId: roomId })

    await new roomModel(room).save()
    return room

  }

  static async list () {

    const roomList: Pick<IRoom, "id" | "name" | "avatar" | "open">[] = await roomModel
      .find({}, "id name avatar open")
      .lean()
    return roomList

  }

  static async checkRoomPlayers (roomId: string): Promise<boolean> {

    const roomHasPlayer = await playerModel.exists({ roomId: roomId })

    if (roomHasPlayer)
      return false

    await roomModel.deleteOne({ id: roomId })
    return true

  }

  static async checkRoomState (
    roomId: string,
    filters: Partial<SetPtiesToAny<IRoom>>&Record<string, any> = {},
    message?: string
  ): Promise<IRoom | null> {

    const room : IRoom | null = await roomModel.findOne({
      id: roomId,
      ...filters
    })

    if (!room && message)
      throw DomainError.message(message)

    return room
  }

  static async changeLeader (roomId: string): Promise<IRoom>
  static async changeLeader (roomId: string, leader?: string): Promise<IRoom> {

    if (!leader) {

      const newLeader: IPlayer | null = await playerModel.findOne({ roomId })
      if (!newLeader)
        throw new UnplainedError("No player found")
      leader = newLeader.id

    }

    const room = await roomModel.findOneAndUpdate(
      { id: roomId },
      { leader },
      { new: true }
    )

    if (!room)
      throw new UnplainedError("Room not found")

    return room

  }

  static async editRules (
    roomId: string,
    rule: Partial<IRoom["rules"]>,
    playerId: string
  ): Promise<IRoom> {

    const [key, value] = Object.entries(rule)[0]
    if (!key || !value)
      throw DomainError.message("Invalid rule: You should provide rule")
    if (!(key in Room.template.rules))
      throw DomainError.message(`Invalid rule: '${key}' rule doesn't exist !`)

    const mongoSet = { $set: { [`rules.${key}`]: rule[key] } }

    const room = await roomModel.findOneAndUpdate(
      { id: roomId, leader: playerId },
      mongoSet,
      { new: true }
    )
    if (!room)
      throw DomainError.message("Your not allowed to edit game rules")
    return room

  }

  static async closeRoomAccess (
    roomId: string,
    playerId: string
  ): Promise<IRoom> {

    const room = await roomModel
      .findOneAndUpdate(
        {
          id: roomId,
          leader: playerId,
          open: true
        },
        { open: false },
        { new: true }
      )
      .lean()

    if (!room)
      throw DomainError.message("Close room refused: You cannot close this room")

    return room

  }

  static async editGameStep (
    step: IRoom["gameStep"],
    roomId: string,
    options: Partial<IRoom> = {},
    filters: Partial<IRoom> = {}
  ): Promise<IRoom> {

    const room: IRoom | null = await roomModel.findOneAndUpdate(
      { id: roomId, gameStep: { $ne: step }, ...filters },
      { gameStep: step, ...options },
      { new: true }
    )
    if (!room)
      throw DomainError.message("Cannot change game step")
    return room

  }

  static async isRoundFinished (roomId: string) : Promise<boolean>{
    const roundNotFinished = await teamModel
      .exists({ roomId: roomId, played: false })
    return !roundNotFinished
  }

  static async nextRound (roomId: string): Promise<IRoom> {

    const roundIncrement = 1
    const room: IRoom | null = await roomModel.findOneAndUpdate(
      { id: roomId, started: true },
      { $inc: { round: roundIncrement } },
      { new: true }
    )
    if (!room)
      throw DomainError.message("Cannot launch new round")
    return room
  }

  static async gameIsFinished (
    roomId: string,
    check: "rounds" | "points" | "all"
  ): Promise<boolean> {

    const room: IRoom | null = await roomModel.findOne({ id: roomId, started: true })
    if (!room)
      throw DomainError.message("Room doesn't exist", "Room.ts/gameIsFinished()")

    const checkPoints = check === "points" || check === "all",
      checkRounds = check === "rounds" || check === "all"

    const [endGameCondtion, endGameQty] = room.rules.endGame

    if (checkRounds && endGameCondtion === "rounds" && room.round >= endGameQty)
      return true

    if (!checkPoints || endGameCondtion !== "points")
      return false

    const teams : ITeam[] = await teamModel.find({ roomId }).lean()

    const allTeamPlayed = teams.some(team => team.played)
    if (!allTeamPlayed)
      return false

    return teams.some(team => team.score >= endGameQty)

  }

  static async nextStep (roomId: string) : Promise<IRoom>{

    const room : IRoom | null = await roomModel.findOne({ id: roomId }) as IRoom
    if (!room)
      throw new UnplainedError("Internal server error: Room does't exist")

    const steps = Object.values(GameStep)
    const prevStepIndex = steps.findIndex(step => step === room.gameStep)
    room.gameStep = steps[prevStepIndex + 1]

    await roomModel.findOneAndUpdate(
      { id: roomId },
      { gameStep: room.gameStep },
      { new: true }
    )

    return room

  }

  static async goToStep (step: string, roomId: string) : Promise<IRoom>{

    const stepExist = GameStep[step]

    if (!stepExist)
      throw DomainError.message("Step jump refused : Step doesn't exist")

    const room: IRoom | null = await roomModel.findOneAndUpdate(
      { id: roomId, gameStep: { $ne: step } },
      { gameStep: step },
      { new: true }
    )

    if (!room)
      throw DomainError.message("Room does't exist")

    return room

  }

}
