import express from "express"
import errorsHandler from "./errorHandlers/apiErrorHandler"
import { routerTest } from "./components/Test/test-router"
import roomsRouter from "./components/Rooms/rooms-router"

const router = express.Router()

router.use("/test", routerTest)
router.use("/rooms", roomsRouter)
router.use(errorsHandler)

export { router as rootRouter }
