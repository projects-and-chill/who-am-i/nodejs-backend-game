import path from "path"

export class UnplainedError extends Error {

  public location: string

  constructor (message: string) {
    super(message)
    this.location = path.resolve("./")
    this.name = "UnplainedError"
  }

}
