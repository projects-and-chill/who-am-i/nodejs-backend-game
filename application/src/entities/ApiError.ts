class ApiError extends Error {

  constructor (
    public title: string,
    message: string,
    public status: number,
    public type: string = "none",
    public instance: string = "none"
  ) {

    super(message)

  }

}

export { ApiError }
