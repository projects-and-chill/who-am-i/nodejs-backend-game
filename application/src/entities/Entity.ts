type EntityProperties<T extends Record<string, any>> = { id: string } & T

export abstract class Entity<Props extends Record<string, any>> {

  static generateId (prefix = ""): string {

    const randomId = () => Math.random().toString(36).substring(2, 9)
    return prefix ? `${prefix}-${randomId()}` : randomId()

  }

  get id (): string {

    return this.properties.id

  }

  get properties (): EntityProperties<Props> {

    return this.props

  }

  protected props: EntityProperties<Props>

  constructor (props: Props, id: string) {

    if (!props.id && !id)
      throw new Error("You should provide entity id")
    this.props = {
      id: props.id || id,
      ...props
    }

  }

}
