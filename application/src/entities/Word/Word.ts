import { Entity } from "../Entity"
import { IWord } from "./IWord"
import roomModel from "../../models/RoomModel"
import { DomainError } from "../DomainError"
import playerModel from "../../models/PlayerModel"
import wordModel from "../../models/WordModel"
import { IRoom } from "../Room/IRoom"
import { IPlayer } from "../Player/IPlayer"
import { ITeam } from "../Team/ITeam"
import { UnplainedError } from "../UnplainedError"
import teamModel from "../../models/TeamModel"

export class Word extends Entity<Word> {

  static template: IWord = {
    id: "",
    roomId: "",
    owner: "",
    value: "",
    guessed: false,
    used: false,
    validate: 0
  }

  static async checkWordState (
    wordId: string,
    filters: Partial<IWord> = {},
    message?: string
  ): Promise<IWord | null> {

    const word : IWord | null = await wordModel.findOne({
      id: wordId,
      ...filters
    })

    if (!word && message)
      throw DomainError.message(message)

    return word
  }

  static async add (
    word: string,
    roomId: string,
    playerId: string,
    playerIsReady: boolean
  ): Promise<{ enough: boolean }> {

    const room: IRoom | null = await roomModel.findOne({ id: roomId })
    if (!room)
      throw DomainError.message("This room doesn't exist")

    const playerBelongRoom = await playerModel.exists({
      id: playerId,
      roomId: roomId
    })
    if (!playerBelongRoom)
      throw DomainError.message("Add word refused: You don't belong to this room")

    const words: IWord[] = await wordModel.find({
      roomId: roomId,
      owner: playerId
    })

    const wordExist = words.find((wrd) => wrd.value === word)
    if (wordExist) {
      throw DomainError.message(
        "Add word refused: You've already provided this word"
      )
    }
    else if (playerIsReady && words.length >= room.rules.wordQty) {
      throw DomainError.message(
        "Add word refused: You've already provided enough words"
      )
    }

    const newWord : IWord = {
      ...Word.template,
      id: Word.generateId("w"),
      roomId: roomId,
      owner: playerId,
      value: word
    }

    await new wordModel(newWord).save()

    const enough = words.length + 1 >= room.rules.wordQty
    return { enough }

  }

  static async remove (
    word: string,
    roomId: string,
    playerId: string
  ): Promise<{ playerUnready: boolean }> {

    const room: IRoom | null = await roomModel.findOne({ id: roomId })
    if (!room)
      throw DomainError.message("This room doesn't exist")

    const player: IPlayer | null = await playerModel.findOne({
      id: playerId,
      roomId: roomId
    })
    if (!player)
      throw DomainError.message("Add word refused: You don't belong to this room")

    await wordModel.deleteOne({
      roomId: roomId,
      owner: playerId,
      value: word
    })

    return { playerUnready: player.vote === 1 }

  }

  static async get (wordId: string, roomId: string): Promise<IWord> {
    const word : IWord | null = await wordModel.findOne({ id: wordId, roomId }).lean()
    if (!word)
      throw DomainError.message("Word doesn't exist")

    return word
  }

  static async change (
    teamId: string,
    roomId: string
  ): Promise<{ word: IWord, team: ITeam }> {

    const blindedPlayer : IPlayer | null = await playerModel.findOne(
      { teamId: teamId, roomId: roomId, blinded: true }
    )

    const filter : Record<string, any> = {
      used: false,
      roomId
    }

    if (blindedPlayer)
      filter["owner"] = { $ne: blindedPlayer.id }

    const count : number = await wordModel.find(filter).count()

    if (!count)
      throw DomainError.message("Oups ! There are no more new words")
    const skip = Math.floor(Math.random() * count)

    const newWord : IWord | null = await wordModel
      .findOne(filter)
      .skip(skip)
      .lean()

    if (!newWord)
      throw DomainError.message("Cannot change word")

    newWord.used = true
    await wordModel.updateOne({ id: newWord.id }, { used: true })

    const team : ITeam | null = await teamModel.findOneAndUpdate(
      { id: teamId },
      { wordId: newWord.id },
      { new: true }
    )

    if (!team)
      throw new UnplainedError("Word selection problem : Internal server error")

    return { word: newWord, team }
  }

  static async validate (wordId: string): Promise<IWord> {
    const word : IWord | null = await wordModel.findOneAndUpdate(
      { id: wordId, validate: 0 },
      { validate: 1 },
      { new: true }
    )
    if (!word)
      throw DomainError.message("Word validation failed: Word doesn't exist")
    return word
  }

  static async unvalidate (wordId: string): Promise<IWord> {
    const word : IWord | null = await wordModel.findOneAndUpdate(
      { id: wordId, validate: 0 },
      { validate: -1 },
      { new: true }
    )

    if (!word)
      throw DomainError.message("Word validation failed: Word doesn't exist")
    return word
  }

  static async guessed (
    wordId: string,
    roomId: string,
    teamId: string
  ): Promise<{ word: IWord, team: ITeam }> {

    const word : IWord | null = await wordModel.findOneAndUpdate(
      { id: wordId, roomId, guessed: false },
      { guessed: true },
      { new: true }
    )
    if (!word)
      throw DomainError.message("Word already guessed")

    const team : ITeam | null = await teamModel.findOneAndUpdate(
      { id: teamId, roomId, wordId },
      { $push: { guessedWords: word.value }, $inc: { score: 1 } },
      { new: true }
    )
    if (!team)
      throw new UnplainedError("Word does't belong to team")

    return { word, team }
  }

}
