import { UnplainedError } from "../entities/UnplainedError"
import { DomainError } from "../entities/DomainError"
import { Socket } from "socket.io"

export function ioErrorHandler (err: any, socket: Socket) {

  console.log("---Error Handler: --- socket :", socket.id)

  if (err instanceof DomainError) {
    console.log("Domain error: ", err.message)
    socket.emit("message", ...["error", err.message])

  }
  else if (err instanceof UnplainedError) {
    console.log("UnplainedError: ", err.message)
    socket.emit("message", ...["error", "An unexpected error has occurred"])
  }
  else {
    console.log("/!\\Other Error /!\\:", err)
    socket.emit("message", ...["error", "Internal server Error"])
  }

}
