import { IRoom } from "../Room/IRoom"

export interface IChrono {
  id: string
  roomId :string
  defaultTime: number
  type: IRoom["rules"]["time"][0]
  state: "play" | "pause"
  startAt: number
  milliseconds: number
}
