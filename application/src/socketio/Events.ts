class EventPrefixer {

  constructor (private prefix: string) {}

  private prefixer (object: Record<string, string>): any {

    const prefixObject: any = {}
    for (const key in object)

      prefixObject[key] = `${this.prefix}:${object[key]}`

    return prefixObject

  }

  public build<
    C extends Record<string, string>,
    S extends Record<string, string>
    > (client: C, server: S) {

    return {
      client: this.prefixer(client) as C,
      server: this.prefixer(server) as S
    }

  }

}

export const roomEvents = new EventPrefixer("room").build(
  {
    LIST: "LIST",
    CREATE: "CREATE",
    EDIT_RULES: "EDIT_RULES",
    CLOSE: "CLOSE",
    GO_TO_STEP: "GO_TO_STEP",
    NEXT_STEP: "NEXT_STEP"
  },
  {
    DELETED: "DELETED",
    CREATED: "CREATED",
    LIST: "LIST",
    LEADER_CHANGE: "LEADER_CHANGE",
    RULES_EDITED: "RULES_EDITED",
    CLOSED: "CLOSED",
    GAME_STARTED: "GAME_STARTED",
    STEP_CHANGED: "STEP_CHANGED",
    NEXT_ROUND: "NEXT_ROUND",
    NEXT_TEAM: "NEXT_TEAM"
  }
)

export const teamEvents = new EventPrefixer("team").build(
  {
    CREATE: "CREATE",
    TURN_END: "TURN_END",
    UPDATE_SCORE: "UPDATE_SCORE",
    GET: "GET"
  },
  {
    CREATED: "CREATED",
    DELETED: "DELETED",
    LIST: "LIST",
    SCORE_UPDATED: "SCORE_UPDATED",
    TURN_ENDED: "TURN_ENDED"
  }
)

export const playerEvents = new EventPrefixer("player").build(
  {
    CREATE: "CREATE",
    JOIN: "JOIN",
    LEAVE: "LEAVE",
    SWITCH_TEAM: "SWITCH_TEAM",
    VOTE_WORD: "VOTE_WORD",
    BLIND: "BLIND"
  },
  {
    JOINED: "JOINED",
    LEAVED: "LEAVED",
    UPDATED: "UPDATED",
    TEAM_SWITCHED: "TEAM_SWITCHED",
    VOTED: "VOTED",
    RESET_VOTE: "RESET_VOTE",
    BLINDED: "BLINDED"
  }
)

export const wordEvent = new EventPrefixer("word").build(
  {
    ADD: "ADD",
    REMOVE: "REMOVE",
    GET: "GET",
    CHANGE: "CHANGE",
    GUESSED: "GUESSED"
  },
  {
    ADDED: "ADDED",
    REMOVED: "REMOVED",
    SHOWN: "SHOWN",
    CHANGED: "CHANGED",
    VALIDATED: "VALIDATED",
    GUESSED: "GUESSED"
  }
)

export const chronoEvents = new EventPrefixer("chrono").build(
  {
    RESET: "RESET",
    INCREMENT: "INCREMENT",
    TOGGLE_STATE: "TOGGLE_STATE",
    CHANGE_TYPE: "CHANGE_TYPE"
  },
  {
    RESETED: "RESETED",
    PAUSE: "PAUSE",
    PLAY: "PLAY",
    INCREMENTED: "INCREMENT",
    TYPE_CHANGE: "TYPE_CHANGE"
  }
)
