import { Entity } from "../Entity"
import { IChrono } from "./IChrono"
import { DomainError } from "../DomainError"
import { IRoom } from "../Room/IRoom"
import { Room } from "../Room/Room"
import chronoModel from "../../models/ChronoModel"

export class Chrono extends Entity<IChrono> {

  static template: IChrono = {
    id: "",
    roomId: "",
    type: "countdown",
    defaultTime: 0,
    state: "pause",
    startAt: 0,
    milliseconds: 0
  }

  static async create (roomId: string): Promise<IChrono> {
    const room = await Room.checkRoomState(
      roomId,
      { started: true, id: roomId },
      "Cannot create chrono, this room does't fill conditions"
    ) as IRoom

    const chrono : IChrono = {
      ...Chrono.template,
      id: Chrono.generateId("c"),
      type: room.rules.time[0],
      // defaultTime: room.rules.time[0] === "countdown" ? room.rules.time[1] * 1000 : 0,
      defaultTime: room.rules.time[1] * 1000,
      roomId
    }
    await chronoModel.updateOne(
      { roomId },
      chrono,
      { upsert: true, new: true }
    )
    return chrono
  }

  static async setType (type: IChrono["type"], chronoId: string, roomId: string): Promise<IChrono> {

    const chrono = await chronoModel.findOneAndUpdate(
      { roomId, chronoId, type: { $ne: type } },
      { type },
      { new: true }
    )
    if (!chrono)
      throw DomainError.message("Cannot change type: Chrono not found")

    return chrono
  }

  static async play (
    chronoId: string,
    roomId: string
  ): Promise<IChrono> {

    const chrono : IChrono | null = await chronoModel.findOneAndUpdate(
      { chronoId, roomId, state: "pause" },
      { startAt: Date.now(), state: "play" },
      { new: true }
    ).lean()

    if (!chrono)
      throw DomainError.message("Cannot play: No paused chrono found is this room")

    return chrono
  }

  static async pause (
    chronoId: string,
    roomId: string
  ): Promise<IChrono> {

    const pauseAt = Date.now()
    const chrono : IChrono | null = await chronoModel.findOne(
      { chronoId, roomId, state: "play" }
    ).lean()

    if (!chrono)
      throw DomainError.message("Cannot pause: No running chrono found is this room")

    const milliseconds = (pauseAt - chrono.startAt) + chrono.milliseconds

    const chronoUpdated = await chronoModel.findOneAndUpdate(
      { chronoId, roomId, state: "play" },
      { pauseAt: pauseAt, state: "pause", milliseconds },
      { new: true }
    ).lean() as IChrono
    if (!chrono)
      throw DomainError.message("Cannot pause chrono now")

    return chronoUpdated
  }

  static async reset (
    chronoId: string,
    roomId: string
  ): Promise<IChrono> {

    const chrono : IChrono | null = await chronoModel.findOne(
      { chronoId, roomId }
    ).lean()

    if (!chrono)
      throw DomainError.message("Cannot find chrono")

    const resetedChrono : IChrono = {
      ...chrono,
      startAt: 0,
      state: "pause",
      // defaultTime: chrono.type === "countdown" ? chrono.defaultTime : 0,
      defaultTime: chrono.defaultTime,
      milliseconds: 0
    }

    await chronoModel.updateOne(
      { chronoId, roomId },
      resetedChrono
    ).lean()

    return resetedChrono
  }

  static async incrementCountDown (
    milliseconds: number|BigInteger,
    chronoId: string,
    roomId: string
  ): Promise<IChrono> {

    const incrementAt = Date.now()

    if (typeof milliseconds != "number")
      throw DomainError.message("Chrono update refused: Please insert valid value")

    const chrono: IChrono | null = await chronoModel
      .findOne({ chronoId, roomId, type: "countdown" })
      .lean()

    if (!chrono)
      throw DomainError.message("Cannot find chrono")

    const spentMilliseconds = (incrementAt - chrono.startAt) + chrono.milliseconds

    let updatedChrono : IChrono | null
    if (chrono.state === "play") {
      updatedChrono = await chronoModel.findOneAndUpdate(
        { chronoId, roomId, state: "play", type: "countdown" },
        {
          milliseconds: spentMilliseconds + (milliseconds * -1),
          startAt: incrementAt
        },
        { new: true }
      ).lean()
    }
    else {
      updatedChrono = await chronoModel.findOneAndUpdate(
        { chronoId, roomId, state: "pause", type: "countdown" },
        { defaultTime: chrono.defaultTime + milliseconds },
        { new: true }
      ).lean()
    }
    if (!updatedChrono)
      throw DomainError.message("Cannot increment chrono now")

    return updatedChrono
  }
}
